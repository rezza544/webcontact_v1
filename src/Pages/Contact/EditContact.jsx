import React from 'react';
import swalert from 'sweetalert';

import HeaderDefault from '../../Components/Header/HeaderDefault';
import TextInput from '../../Components/TextInput/TextInput';
import Avatar from '@material-ui/core/Avatar';

import { GETCONTACT } from '../../Services/Services';
import { Redirect } from 'react-router-dom';

const MAX_WIDTH = 320;
const MAX_HEIGHT = 180;
const MIME_TYPE = "image/jpeg";
const QUALITY = 0.7;

class EditContact extends React.Component {
    constructor(props) {
        super(props);
        this.state = {


            colorBottomFirstName: false,
            colorBottomLastName: false,
            colorBottomAge: false,

            /**
             * STATE FROM LIST CONTACT
             */
            // docnumParams: this.props.location.state.docnum,
            id: this.props.location.state.id,
            firstname: this.props.location.state.firstName,
            lastname: this.props.location.state.lastName,
            age: this.props.location.state.age,
            photo: this.props.location.state.photo,

            redirect: '',
        }
    }


    componentDidMount() {
        console.log(this.state.firstname);
    }

    // === RESIZE ====

    calculateSize(img, maxWidth, maxHeight) {
        let width = img.width;
        let height = img.height;
    
        // calculate the width and height, constraining the proportions
        if (width > height) {
            if (width > maxWidth) {
                height = Math.round((height * maxWidth) / width);
                width = maxWidth;
            }
        } else {
            if (height > maxHeight) {
                width = Math.round((width * maxHeight) / height);
                height = maxHeight;
            }
        }
        return [width, height];
    }

    handleUploadImage(ev) {
        const file = ev.target.files[0]; // get the file
        const blobURL = URL.createObjectURL(file);
        const img = new Image();
        img.src = blobURL;
        img.onerror = function () {
            URL.revokeObjectURL(this.src);
            // Handle the failure properly
            console.log("Cannot load image");
        };
        img.onload = () => {
            URL.revokeObjectURL(this.src);
            const [newWidth, newHeight] = this.calculateSize(img, MAX_WIDTH, MAX_HEIGHT);
            const canvas = document.createElement("canvas");
            canvas.width = newWidth;
            canvas.height = newHeight;
            const ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0, newWidth, newHeight);
            canvas.toBlob(
            (blob) => {
                // Handle the compressed image. es. upload or save in local state
                console.log(file);
                console.log(blob);
            },
            MIME_TYPE,
            QUALITY
            );
            console.log(canvas.toDataURL("image/jpeg", 0.7));
            this.setState({
                photo: canvas.toDataURL("image/jpeg", 0.7)
            });
            
        };
    }

    handleTextInputFirstName = async(value) => {
        try {
            this.setState({ firstname: value });

            if(value.toString().length > 0) {
                this.setState({
                    colorBottomFirstName: false
                });
            }else {
                this.setState({
                    colorBottomFirstName: true
                })
            }
        } catch (err) {
            console.log(err);
        }
    }

    handleTextInputLastName = async(value) => {
        try {
            this.setState({ lastname: value });

            if(value.toString().length > 0) {
                this.setState({
                    colorBottomLastName: false
                });
            }else {
                this.setState({
                    colorBottomLastName: true
                })
            }
        } catch (err) {
            console.log(err);
        }
    }

    handleTextInputAge = async(value) => {
        try {
            this.setState({ age: value });

            if(value.toString().length > 0) {
                this.setState({
                    colorBottomAge: false
                });
            }else {
                this.setState({
                    colorBottomAge: true
                });
            }
        } catch (err) {
            console.log(err);
        }
    }

    handleUpdateContact = async() => {
        try {
            console.log(JSON.stringify({
                firstName: this.state.firstname,
                lastName: this.state.lastname,
                age: this.state.age,
                photo: this.state.photo,
            }));

            if(this.state.photo === '' || this.state.firstname.toString().length <= 0 || this.state.lastname.toString().length <= 0 || this.state.age.toString().length <= 0) {
                this.setState({
                    colorBottomFirstName: true,
                    colorBottomLastName: true,
                    colorBottomAge: true
                });

                
                return swalert({
                    title: 'Information',
                    text: 'Sorry, input fields and photo cannot be empty'
                });

            }

            const postUrl = await fetch(`${GETCONTACT}/${this.state.id}`, {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify({
                    firstName: this.state.firstname,
                    lastName: this.state.lastname,
                    age: this.state.age,
                    photo: this.state.photo,
                })
            });

            const resJson = await postUrl.json();

            if(resJson.statusCode === undefined || resJson.statusCode === "") {
                swalert({
                    title: 'Success',
                    icon: 'success',
                    text: resJson.message,
                    buttons: {
                        oke: {
                            text: 'Oke',
                            value: 'oke'
                        }
                    },
                    closeOnClickOutside: false
                }).then((value) => {
                    if(value === 'oke') {
                        this.setState({ redirect: '/ListContact' })
                    }
                });
            }else {
                swalert({   
                    title: 'Error',
                    text: resJson.message,
                    icon: 'error'
                });

            }

            console.log(resJson);
        } catch (err) {
            console.log(err);
        }
    }


    render() {
        if(this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }
        return (
            <div id="main-detailContact__" className="main-detailContact">
                <HeaderDefault 
                    onClickRight={() => this.handleUpdateContact()}
                    to={'/ListContact'}
                    title={'UPDATE'}
                />

                <div className="c-content-body-detailContact__">
                    <div className="c-content-body___">
                        <div className="c-photo-detailContact__">
                            <label htmlFor="file-input">
                                <Avatar 
                                    className="img-contact"
                                    src={this.state.photo}
                                ></Avatar>
                            </label>

                            <input 
                                id="file-input" 
                                type="file" 
                                onChange={(value) => this.handleUploadImage(value)}
                            />
                        </div>

                        <div className="c-inputs-detailContact__">
                            <TextInput 
                                title={'First Name'}
                                defaultValue={this.state.firstname}
                                onChange={(value) => this.handleTextInputFirstName(value.target.value)}
                                colorBottom={this.state.colorBottomFirstName}
                            />
                            <TextInput 
                                title={'Last Name'}
                                defaultValue={this.state.lastname}
                                onChange={(value) => this.handleTextInputLastName(value.target.value)}
                                colorBottom={this.state.colorBottomLastName}
                            />
                            <TextInput 
                                title={'Age'}
                                defaultValue={this.state.age}
                                onChange={(value) => this.handleTextInputAge(value.target.value)}
                                colorBottom={this.state.colorBottomAge}
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default EditContact;