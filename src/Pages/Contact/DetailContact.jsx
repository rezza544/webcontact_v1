import React from 'react';

import HeaderDefault from '../../Components/Header/HeaderDefault';
import TextInput from '../../Components/TextInput/TextInput';
import Avatar from '@material-ui/core/Avatar';

import Photo from '../../Utils/Images/dochi.jpg';

class DetailContact extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }


    render() {
        return (
            <div id="main-detailContact__" className="main-detailContact">
                <HeaderDefault 
                    to={'/ListContact'}
                    title={'SAVE'}
                />

                <div className="c-content-body-detailContact__">
                    <div className="c-content-body___">
                        <div className="c-photo-detailContact__">
                            <Avatar 
                                className="img-contact"
                                src={Photo}
                            ></Avatar>
                        </div>

                        <div className="c-inputs-detailContact__">
                            <TextInput 
                                title={'First Name'}
                            />
                            <TextInput 
                                title={'Last Name'}
                            />
                            <TextInput 
                                title={'Age'}
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default DetailContact;