import React from 'react';
import swalert from 'sweetalert';

import HeaderDefault from '../../Components/Header/HeaderDefault';
import ListViewDefault from '../../Components/ListView/ListViewDefault';
import Button from '../../Components/Button/Button';

import { Collapse } from 'react-bootstrap';
import { GETCONTACT } from '../../Services/Services';

class ListContact extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            collapse: null,

            dataDummy: [
                {
                    nama: "reza",
                    phone: "0848484844452",
                }, {
                    nama: "alfian",
                    phone: "0824828428424"
                }
            ],

            dataContact: [],
            
        }
        this.handleOpenCollapse = this.handleOpenCollapse.bind(this);
    }

    componentDidMount() {
        this.handleGetContact();
    }

    handleOpenCollapse = (index) => {
        try {
            this.setState({
                collapse: this.state.collapse === index ? null : index
            });
            console.log(this.state.collapse);
        } catch (err) {
            console.log(err);
        }
    }

    handleGetContact = async() => {
        try {
            const getUrl = await fetch(GETCONTACT, {
                method: "GET"
            });

            const resJson = await getUrl.json();

            console.log(resJson);

            if(resJson.statusCode === undefined || resJson.statusCode === "") {
                this.setState({
                    dataContact: resJson.data
                });
            }else {
                console.log(resJson);
                alert(resJson.message);
            }
        } catch (err) {
            console.log(err);
        }
    }

    handleDeleteContact = async(idUser) => {
        try {
            const deleteUrl = await fetch(`${GETCONTACT}/${idUser}`, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });

            const resJson = await deleteUrl.json();

            console.log(resJson);
            console.log(resJson.message);

            if(resJson.statusCode === undefined || resJson.statusCode === "") {
                console.log(resJson);
                // this.setState({
                //     redirect: '/ListContact'
                // })
                window.location.reload(false);
            }else {
                swalert({   
                    title: 'Error',
                    text: resJson.message,
                    icon: 'error'
                });
            }
        } catch (err) {
            console.log(err);
            swalert({   
                title: 'Error',
                text: err,
                icon: 'error'
            });
        }
    }

    render() {
        return (
            <div id="main-listContact" className="c-listContact">
                <HeaderDefault 
                    to={'/ListContact'}
                    title={'CREATE'}
                    toRight={'/CreateContact'}
                />

                <div className="c-listContact-ListView">
                    <div className="c-listContact-body__">
                        {
                            this.state.dataContact.map((value, key) => (
                                <div
                                    key={key}
                                >
                                    <ListViewDefault 
                                        onClick={() => this.handleOpenCollapse(key)}
                                        title={value.firstName}
                                        collapse={this.state.collapse === key}
                                        uri={value.photo}
                                    />
                                    <Collapse
                                        in={this.state.collapse === key}
                                    >
                                        <div
                                            className='c-expand__'
                                        >
                                            <div className="data-collapse__">
                                                <p>First Name:</p>
                                                <p className="title-data__"><strong> {value.firstName}</strong></p>
                                            </div>

                                            <div className="data-collapse__">
                                                <p>Last Name:</p>
                                                <p className="title-data__"><strong> {value.lastName}</strong></p>
                                            </div>

                                            <div className="data-collapse__">
                                                <p>Age:</p>
                                                <p className="title-data__"><strong> {value.age}</strong></p>
                                            </div>

                                            <div className="data-collapse-actions__">
                                                <Button 
                                                    to={{
                                                        pathname: `/EditContact/${value.id}`,
                                                        state: {
                                                            id: value.id,
                                                            firstName: value.firstName,
                                                            lastName: value.lastName,
                                                            age: value.age,
                                                            photo: value.photo
                                                        }

                                                    }}
                                                    typeButton={'edit'}
                                                    title={'EDIT'}
                                                />
                                                <Button 
                                                    onClick={() => this.handleDeleteContact(value.id)}
                                                    marginLeft={10}
                                                    typeButton={'delete'}
                                                    title={'DELETE'}
                                                />
                                            </div>
                                        </div>
                                    </Collapse>
                                </div>
                            ))
                        }

                    </div>
                </div>
            </div>
        )
    }
}

export default ListContact;