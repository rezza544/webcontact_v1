import React from 'react';
import './App.css';
// import logo from './logo.svg';

import { HashRouter as Router, Route } from 'react-router-dom';

/**
 * CONTACT PAGES
 * ===============================================================================================
 */
import ListContact from './Pages/Contact/ListContact';
import DetailContact from './Pages/Contact/DetailContact';
import EditContact from './Pages/Contact/EditContact';
import CreateContact from './Pages/Contact/CreateContact';



function App() {
  return (
    <Router>
      <Route path="/" exact component={ListContact} />
      <Route path="/ListContact" component={ListContact} />
      <Route path="/DetailContact" component={DetailContact} />
      <Route path="/EditContact" component={EditContact} />
      <Route path="/CreateContact" component={CreateContact} />
    </Router>
  );
}

export default App;
