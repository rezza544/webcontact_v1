import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';


import 'bootstrap/dist/css/bootstrap.css';

/**
 * PAGES STYLES SCSS
 * ================================================================================
 */
import './Utils/Styles/Pages/Contact/ListContact.scss';
import './Utils/Styles/Pages/Contact/DetailContact.scss';

/**
 * COMPONENT STYLES SCSS
 * =================================================================================
 */
import './Utils/Styles/Components/HeaderDefault.scss';
import './Utils/Styles/Components/ListView/ListViewDefault.scss';
import './Utils/Styles/Components/DetailContact/DetailContact.scss';
import './Utils/Styles/Components/Button/Button.scss';
import './Utils/Styles/Components/TextInput/TextInput.scss';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
