const BASE_URL = `https://simple-contact-crud.herokuapp.com`;

/**
 * API URL CONTACT
 * ====================================================================================
 */
export const GETCONTACT = `${BASE_URL}/contact`;