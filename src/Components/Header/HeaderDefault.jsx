import React from 'react';
import Button from '../Button/Button';

import { Link } from 'react-router-dom';

class HeaderDefault extends React.Component {
    render() {
        return (
            <div className="header-default">
                <div className="main-header-default">
                    <div className="c-content-header-left">
                        <Link
                            to={this.props.to}
                            onClick={this.props.onClick}
                            className="title-header"
                        >
                            <strong>
                                CONTACT
                            </strong>
                        </Link>
                    </div>

                    <div className="c-content-header-right">
                        <Button
                            to={this.props.toRight}
                            onClick={this.props.onClickRight}
                            typeButton={'create'}
                            title={this.props.title}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default HeaderDefault;