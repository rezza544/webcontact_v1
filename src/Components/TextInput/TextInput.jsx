import React from 'react';

class TextInput extends React.Component {
    render() {
        return (
            <div className="main-textinput">
                <p>
                    <strong>
                        {this.props.title}
                    </strong>
                </p>
                <input
                    className={this.props.colorBottom ? 'colorBottomInputFalse' : 'colorBottomInputTrue'}
                    placeholder={this.props.placeholder} 
                    type={this.props.type}
                    defaultValue={this.props.defaultValue}
                    onChange={this.props.onChange}
                />
            </div>
        )
    }
}

export default TextInput;