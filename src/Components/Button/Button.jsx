import React from 'react';
import { Link } from 'react-router-dom';

class Button extends React.Component {
    render() {
        return (
            <Link
                to={this.props.to}
                onClick={this.props.onClick}
                style={{
                    ...this.props
                }}
                className={
                    this.props.typeButton === 'edit' ? "main-button btn__ btn-edit" : 
                    this.props.typeButton === 'delete' ? 'main-button btn__ btn-delete' : 
                    this.props.typeButton === 'create' ? 'main-button btn__ btn-create' : ''}
            >
                <p>
                    <strong>
                        {this.props.title}
                    </strong>
                </p>
            </Link>
        )
    }
}

export default Button;