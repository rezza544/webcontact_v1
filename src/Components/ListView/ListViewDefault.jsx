import React from 'react';
import Avatar from '@material-ui/core/Avatar';

import I_Expand from '../../Utils/Images/Icon/icon_expand.png';

class ListViewDefault extends React.Component {
    render() {
        return (
            <div 
                className="c-listview-default" 
                onClick={this.props.onClick}
                aria-expanded="false"
                aria-controls="collapseExample"
            >
                <div className="c-content-list-left">
                    <Avatar 
                        className="img-listview"
                        src={this.props.uri}
                    ></Avatar>

                    <p
                        className="title-listview"
                    >
                        <strong>
                            {this.props.title}
                        </strong>
                    </p>
                </div>

                <div className="c-content-list-right">
                    <div className={this.props.collapse ? "i-expand i-icon" : "i-icon" }>
                        <img 
                            src={I_Expand} 
                            alt="expand icon" 
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default ListViewDefault;